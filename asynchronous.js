/* Name : asynchronous.js
 *  Description : javascript to order the data using callback function and Promises..
 *  Input : List of file names
 *  Output : it prints the required files in the order they were demanded
 *  usage : node asynchronous.js
 *	author: Satyansh Sagar
 */

//filename array to hold the names of the file requested
//fileFlag to store the flag value for every file to check if it's printed or not
var filename=[];
var fileFlag=[];
//sequence variable to ensure the order of printing and value_count to count the number of value we have got from fakeAjax function
var sequence=0,value_count=0;
var temp=[];

function fakeAjax(url,cb) {
	var fake_responses = {
		"file1": "The first text",
		"file2": "The middle text",
		"file3": "The last text"
	};
	var randomDelay = (Math.round(Math.random() * 1E4) % 8000) + 1000;

	console.log("Requesting: " + url);
	setTimeout(function(){
		cb(fake_responses[url],url);
	},randomDelay);
}

function getFile(file) {
	filename.push(file);
	fileFlag.push(0);
	fakeAjax(file,callback);
}

function callback(value,text){
	var current = filename.indexOf(text);
	console.log(current);
		//checking the sequence is equal to current value or not
		if(sequence===current){
			fileFlag[current]=1;
			sequence++;
			value_count++;
			//if any other values are stored..printing them from temp array
			for (var i = 0; i < filename.length; i++) {
						if(i===sequence && typeof temp[i]!=='undefined'){
							sequence++;
							fileFlag[i]=1;
							console.log(temp[i]);
						}}

			}
		else {
			//push the element to tamp array
			temp[current] = value;
			value_count++;
		}

		if(value_count===fileFlag.length) {

			//printing the remaining elements
				for (var i = 0; i < filename.length; i++) {
							if(fileFlag[i]===0){
								console.log(temp[i]);
							}
				}
		}

}
// request all files at once in "parallel"
getFile("file1");
getFile("file2");
getFile("file3");
